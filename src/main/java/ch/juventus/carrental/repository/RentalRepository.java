package ch.juventus.carrental.repository;

import ch.juventus.carrental.model.Rental;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/** Represents Rental Repository that extends JpaRepo
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Repository
public interface RentalRepository extends JpaRepository<Rental, Long> {
}

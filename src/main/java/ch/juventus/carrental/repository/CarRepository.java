package ch.juventus.carrental.repository;

import ch.juventus.carrental.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/** Represents Car Repository that extends JpaRepo
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
}

package ch.juventus.carrental.dto;

import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.GearShift;
import ch.juventus.carrental.model.Type;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/** Represents Car Data transfer object
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public class CarDto {
    @NotEmpty(message = "The car name is required.")
    @Size(min = 2, max = 100, message = "The length of car name must be between 2 and 100 characters.")
    private String name;

    @DecimalMin(value = "0.0")
    private double pricePerDay;

    @DecimalMin(value = "0")
    private int seats;

    private boolean airCondition;

    @NotEmpty(message = "The car type is required.")
    @Size(min = 2, max = 100, message = "The length of car type must be between 2 and 100 characters.")
    private String type;

    @NotEmpty(message = "The car gearShift is required.")
    @Size(min = 2, max = 100, message = "The length of car gearShift must be between 2 and 100 characters.")
    private String gearShift;

    public Car toCar() {
        Car car = new Car();
        car.setName(name);
        car.setAirCondition(airCondition);
        car.setPricePerDay(pricePerDay);
        car.setSeats(seats);
        for (GearShift gs : GearShift.values()) {
            if (gs.name().equals(gearShift)) {
                car.setGearShift(gs);
            }
        }

        for (Type type : Type.values()) {
            if (type.name().equals(this.type)) {
                car.setType(type);
            }
        }


        //validate enums and set here
        return car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(double pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public boolean isAirCondition() {
        return airCondition;
    }

    public void setAirCondition(boolean airCondition) {
        this.airCondition = airCondition;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGearShift() {
        return gearShift;
    }

    public void setGearShift(String gearShift) {
        this.gearShift = gearShift;
    }
}

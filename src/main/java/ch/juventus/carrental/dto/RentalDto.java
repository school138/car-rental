package ch.juventus.carrental.dto;

import ch.juventus.carrental.model.Rental;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

/** Represents Rental Data transfer object
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public class RentalDto {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    public Rental toRental() {
        Rental rental = new Rental();
        rental.setStartDate(startDate);
        rental.setEndDate(endDate);
        return rental;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }
}

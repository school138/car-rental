package ch.juventus.carrental.filter;

import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.service.RentalDateValidatorService;

import java.time.LocalDate;
/** Represents CarFilterEvaluator
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public class CarFilterEvaluator {

    /** Checks all filters from Car and if the chosen Date is Rentable
     *
     * @param car
     * @param carFilter
     * @param localDate
     * @return true if both car and date matches
     */
    public static boolean isCarAndDateMatch(Car car, CarFilterDto carFilter, LocalDate localDate) {
        if (!isCarMatch(car, carFilter)) {
            return false;
        }

        if (!RentalDateValidatorService.isDateRentable(car, localDate)) {
            return false;
        }

        return true;
    }

    /** Checks all car filters and if the dates are not rented
     *
     * @param car
     * @param carFilter
     * @param startDate
     * @param endDate
     * @return true if both car and the dates match
     */
    public static boolean isCarAndDatesMatch(Car car, CarFilterDto carFilter, LocalDate startDate, LocalDate endDate) {
        if (!isCarMatch(car, carFilter)) {
            return false;
        }

        if (!RentalDateValidatorService.areDatesRentable(car, startDate, endDate)) {
            return false;
        }

        return true;
    }

    /** Checks if  car filters match with saved cars
     *
     * @param car
     * @param carFilter
     * @return true if the filters are a match
     */
    public static boolean isCarMatch(Car car, CarFilterDto carFilter) {
        if (!isMatchForAirCondition(car, carFilter)) {
            return false;
        }

        if (!isMatchForSeats(car, carFilter)) {
            return false;
        }

        if (!isMatchForGearShift(car, carFilter)) {
            return false;
        }

        if (!isMatchForType(car, carFilter)) {
            return false;
        }

        if (!isMatchForSearchQuery(car, carFilter)) {
            return false;
        }

        if (!isMatchForMaxPrice(car, carFilter)) {
            return false;
        }

        if (!isMatchForMinPrice(car, carFilter)) {
            return false;
        }

        return true;
    }

    /** Checks the given max Price per day
     *
     * @param car
     * @param carFilter
     * @return filtered cars with given min Price per day
     */
    private static boolean isMatchForMaxPrice(Car car, CarFilterDto carFilter) {
        if (carFilter.getMaxPricePerDay() == null) {
            return true;
        }
        return carFilter.getMaxPricePerDay() >= car.getPricePerDay();
    }

    /** Checks the given min Price per day
     *
     * @param car
     * @param carFilter
     * @return filtered cars with given min Price per day
     */
    private static boolean isMatchForMinPrice(Car car, CarFilterDto carFilter) {
        if (carFilter.getMinPricePerDay() == null) {
            return true;
        }
        return carFilter.getMinPricePerDay() <= car.getPricePerDay();
    }

    /** Checks the AirCondition for filtered cars
     *
     * @param car
     * @param carFilter
     * @return filtered cars with given AirCondition
     */
    private static boolean isMatchForAirCondition(Car car, CarFilterDto carFilter) {
        if (carFilter.getAirCondition() == null) {
            return true;
        }
        return carFilter.getAirCondition().equals(car.isAirCondition());
    }

    /** Checks filtered seats of cars
     *
     * @param car
     * @param carFilter
     * @return filtered cars with given seats
     */
    private static boolean isMatchForSeats(Car car, CarFilterDto carFilter) {
        if (carFilter.getSeats() == null) {
            return true;
        }
        return carFilter.getSeats().contains(car.getSeats());
    }

    /** Checks filtered GearShift of cars
     *
     * @param car
     * @param carFilter
     * @return filterd cars with given GearShift
     */
    private static boolean isMatchForGearShift(Car car, CarFilterDto carFilter) {
        if (carFilter.getGearShift() == null) {
            return true;
        }
        return carFilter.getGearShift().equals(car.getGearShift());
    }

    /** Checks filtered Type of cars
     *
     * @param car
     * @param carFilter
     * @return filtered cars with given Type
     */
    private static boolean isMatchForType(Car car, CarFilterDto carFilter) {
        if (carFilter.getType() == null) {
            return true;
        }
        return carFilter.getType().contains(car.getType());
    }

    /** Checks filtered searchQuery of cars
     *
     * @param car
     * @param carFilter
     * @return filtered cars with searchQuery
     */
    private static boolean isMatchForSearchQuery(Car car, CarFilterDto carFilter) {
        if (carFilter.getSearchQuery() == null) {
            return true;
        }
        return car.getName().toLowerCase().contains(carFilter.getSearchQuery().toLowerCase());
    }

}

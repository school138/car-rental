package ch.juventus.carrental.service;

import ch.juventus.carrental.controller.CarController;
import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.filter.CarFilterEvaluator;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.repository.CarRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/** Represents CarServiceImpl
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Service
public class CarServiceImpl implements CarService {

    private static final Logger logger = LoggerFactory.getLogger(CarServiceImpl.class);

    private final CarRepository carRepository;

    private final RentalService rentalService;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, RentalService rentalService) {
        this.carRepository = carRepository;
        this.rentalService = rentalService;
    }

    /** Gets all Cars
     *
     * @return all Cars
     */
    @Override
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    /** Gets Car with given Id
     *
     * @param id
     * @return the Car with given Id
     */
    @Override
    public Optional<Car> getCar(Long id) {
        return this.carRepository.findById(id);
    }

    /** Saves a car
     *
     * @param car
     * @return the saved car
     */
    @Override
    public Car saveCar(Car car) {
        return this.carRepository.save(car);
    }

    /** Deletes Car by its Id
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        Optional<Car> carToDelete = this.getCar(id);
        if (carToDelete.isPresent()) {
            carToDelete.get().getRentals().forEach(x -> this.rentalService.deleteById(x.getId()));
            this.carRepository.deleteById(id);
        }
    }

    /** searches car based on given filters
     *
     * @param carFilter
     * @return all cars
     */
    @Override
    public List<Car> searchCar(CarFilterDto carFilter) {
        return carRepository.findAll().stream().filter(x -> CarFilterEvaluator.isCarMatch(x, carFilter)).collect(Collectors.toList());
    }

    /** searches cars that are free to rent with given start and end date
     *
     * @param carFilter
     * @param startDate
     * @param endDate
     * @return all cars with two dates that are free to rent
     */
    @Override
    public List<Car> searchCarWithTwoDates(CarFilterDto carFilter, LocalDate startDate, LocalDate endDate) {
        return carRepository.findAll().stream().filter(x -> CarFilterEvaluator.isCarAndDatesMatch(x, carFilter, startDate, endDate)).collect(Collectors.toList());
    }

    /** searches cars that are free to rent either with given start or end date
     *
     * @param carFilter
     * @param localDate
     * @return all cars with one date that are free to rent
     */
    @Override
    public List<Car> searchCarWithOneDate(CarFilterDto carFilter, LocalDate localDate) {
        return carRepository.findAll().stream().filter(x -> CarFilterEvaluator.isCarAndDateMatch(x, carFilter, localDate)).collect(Collectors.toList());
    }

    /** searches cars based on given attributes and based on if they are free at the given dates
     *
     * @param carFilterDto
     * @param startDate
     * @param endDate
     * @return the cars who are matched
     */
    @Override
    public List<Car> searchCarMaster(CarFilterDto carFilterDto, LocalDate startDate, LocalDate endDate) {



        //1.Case when two dates are given
        if (startDate != null && endDate != null) {
            if (!RentalDateValidatorService.areDatesValid(startDate, endDate)) {
                return List.of();
            }
            return searchCarWithTwoDates(carFilterDto, startDate, endDate);
        }

        //2 Case: when only startdate is given
        if (startDate != null) {
            if (!startDate.isAfter(LocalDate.now())) {
                return List.of();
            }
            return searchCarWithOneDate(carFilterDto, startDate);
        }

        //3 Case: when only enddate is given
        if (endDate != null) {
            if (!endDate.isAfter(LocalDate.now())) {
                return List.of();
            }
            return searchCarWithOneDate(carFilterDto, endDate);
        }

        //4 Case: when no dates are given
        return searchCar(carFilterDto);
    }


}

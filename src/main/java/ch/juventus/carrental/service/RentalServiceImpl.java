package ch.juventus.carrental.service;

import ch.juventus.carrental.dto.RentalDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Rental;
import ch.juventus.carrental.repository.RentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

/** Represents RentalServiceImpl
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Service
public class RentalServiceImpl implements RentalService {


    private RentalRepository rentalRepository;

    @Autowired
    public RentalServiceImpl(RentalRepository rentalRepository) {
        this.rentalRepository = rentalRepository;

    }


    /** Saves Rental
     *
     * @param rental
     * @return saved Rental
     */
    @Override
    public Rental saveRental(Rental rental) {
        return this.rentalRepository.save(rental);
    }

    /** Deletes Rental with given Id
     *
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        this.rentalRepository.deleteById(id);
    }

    /** Creates Rental with calculated price and the rented time period
     *
     * @param rentalDto
     * @param car
     * @return the created rental object
     */
    @Override
    public Rental createRentalForCar(RentalDto rentalDto, Car car) {
        Rental rental = rentalDto.toRental();
        rental.setCar(car);
        double calculatedPrice = calculateTotalPrice(car.getPricePerDay(), rental.getStartDate(), rental.getEndDate());
        rental.setTotalPrice(calculatedPrice);
        return rental;
    }

    /**
     *
     * @param pricePerDay
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public Double calculateTotalPrice(Double pricePerDay, LocalDate startDate, LocalDate endDate) {
        long daysBetween = DAYS.between(startDate, endDate.plusDays(1));
        return daysBetween * pricePerDay;
    }
}

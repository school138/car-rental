package ch.juventus.carrental.service;


import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.model.Car;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
/** Represents CarService Interface
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Repository
public interface CarService {

     List<Car> getAllCars();

     Optional<Car> getCar(Long id);

     Car saveCar(Car car);

     void deleteById(Long id);

     List<Car> searchCar(CarFilterDto carFilter);

     List<Car> searchCarWithOneDate(CarFilterDto carFilter, LocalDate localDate);

     List<Car> searchCarWithTwoDates(CarFilterDto carFilter, LocalDate startDate, LocalDate endDate);

     List<Car> searchCarMaster(CarFilterDto carFilter, LocalDate startDate, LocalDate endDate);

}

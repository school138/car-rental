package ch.juventus.carrental.service;

import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Rental;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
/** Represents RentalDateValidatorService
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public class RentalDateValidatorService {

    private static final Logger logger = LoggerFactory.getLogger(RentalDateValidatorService.class);

    /** checks if Car is Rentable
     *
     * @param car
     * @param startDate
     * @param endDate
     * @return false when dates are not Valid
     */
    public static boolean isRentable(Car car, LocalDate startDate, LocalDate endDate) {
        if (!areDatesValid(startDate, endDate)) {
            return false;
        }
        return areDatesRentable(car, startDate, endDate);
    }

    /** Checks if Dates are Rentable
     *
     * @param car
     * @param startDate
     * @param endDate
     * @return false when date is already rented
     */
    public static boolean areDatesRentable(Car car, LocalDate startDate, LocalDate endDate) {

        for (Rental rental : car.getRentals()) {
            LocalDate rentalStart = rental.getStartDate();
            LocalDate rentalEnd = rental.getEndDate();

            List<LocalDate> rentedDays = rentalStart.datesUntil(rentalEnd.plusDays(1)).collect(Collectors.toList());

            List<LocalDate> daysToCheck = startDate.datesUntil(endDate.plusDays(1)).collect(Collectors.toList());

            for (LocalDate date : daysToCheck) {
                if (rentedDays.contains(date)) {
                    logger.info("Car is not rentable on given Timeframe: " + startDate + " till " + endDate );
                    return false;
                }
            }

        }
        logger.info("Car is rentable on given Dates: " + startDate + " till " + endDate );
        return true;
    }

    /** Checks if date is Rentable
     *
     * @param car
     * @param localDate
     * @return false when rented days contain local Date
     */
    public static boolean isDateRentable(Car car, LocalDate localDate) {

        for (Rental rental : car.getRentals()) {
            LocalDate rentalStart = rental.getStartDate();
            LocalDate rentalEnd = rental.getEndDate();

            List<LocalDate> rentedDays = rentalStart.datesUntil(rentalEnd.plusDays(1)).collect(Collectors.toList());

            if (rentedDays.contains(localDate)) {
                logger.info("Car is already booked on given Date: " + localDate);
                return false;
            }
        }
        logger.info("Car is rentable on given Date: " + localDate);
        return true;
    }

    /** checks if dates are Valid by checking if both dates are given and endDate is after StartDate and if one of the dates is in the past
     *
     * @param startDate
     * @param endDate
     * @return  true when all conditions are met
     */
    public static boolean areDatesValid(LocalDate startDate, LocalDate endDate) {
        if (startDate == null || endDate == null) {
            logger.warn("Both dates have to be given. Either both or one is missing.");
            return false;
        }

        if (endDate.isBefore(startDate)) {
            logger.warn("End Date can not be before Start Date!");
            return false;
        }

        if (LocalDate.now().isAfter(startDate) || LocalDate.now().isAfter(endDate)) {
            logger.warn("Neither of the Dates are allowed to be in the past!");
            return false;
        }
        logger.info("Dates are valid!");
        return true;
    }

}

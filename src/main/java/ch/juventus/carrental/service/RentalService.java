package ch.juventus.carrental.service;


import ch.juventus.carrental.dto.RentalDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Rental;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

/** Represents RentalService Interface
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@Repository
public interface RentalService {

    Rental saveRental(Rental rental);

    void deleteById(Long id);

    Double calculateTotalPrice(Double pricePerDay, LocalDate startDate, LocalDate endDate);

    Rental createRentalForCar(RentalDto rentalDto, Car car);
}

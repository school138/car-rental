package ch.juventus.carrental.controller;

import ch.juventus.carrental.dto.CarDto;
import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.dto.RentalDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Rental;
import ch.juventus.carrental.service.CarService;
import ch.juventus.carrental.service.RentalDateValidatorService;
import ch.juventus.carrental.service.RentalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class CarController {

    private static final Logger logger = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private CarService carService;

    @Autowired
    private RentalService rentalService;

    @Operation(summary = "Get all Cars")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found Cars",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Car.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "No Cars found",
                    content = @Content) })
    @GetMapping("/cars")
    List<Car> getAllCars() {
        logger.info("Getting all cars");
        return this.carService.getAllCars();
    }


    @Operation(summary = "Get a Car by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the Car",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Car.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Car not found",
                    content = @Content) })
    @GetMapping("/car/{id}")
    public ResponseEntity<Car> getCarById(@Parameter(description = "id of Car to be searched")@PathVariable("id") long id) {
        logger.info("Trying to get car with Id: "+ id);
        Optional<Car> car = this.carService.getCar(id);

        if (car.isPresent()) {
            logger.info("Car with Id: "+ id + " was found." );
            return new ResponseEntity<>(car.get(), HttpStatus.OK);
        } else {
            logger.info("Car with Id: "+ id + " could not be found.");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Creates a Car")
    @PostMapping("/car")
    public ResponseEntity<Car> createCar(@Valid @RequestBody CarDto carDto) {
        logger.info("Creating Car");
        Car car = carDto.toCar();
        return new ResponseEntity<>(this.carService.saveCar(car), HttpStatus.CREATED);
    }

    @Operation(summary = "Deletes a Car")
    @DeleteMapping("/car/{id}")
    public ResponseEntity<HttpStatus> deleteCar(@PathVariable("id") long id) {
        logger.info("Trying to delete car with Id: "+ id);
        try {
            this.carService.deleteById(id);
            logger.info("Car with Id: "+ id + " was deleted." );
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Exception happened while trying to delete car with id:" + id);
            logger.error("",e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    @Operation(summary = "Updates an existing Car")
    @PutMapping("/car/{id}")
    public ResponseEntity<Car> updateCar(@PathVariable("id") long id, @Valid @RequestBody CarDto carDto) {
        logger.info("Trying to update car with id: "+ id);
        Optional<Car> carToUpdate = this.carService.getCar(id);

        if (carToUpdate.isPresent()) {
            Car car = carDto.toCar();
            car.setId(id);
            logger.info("updated car with Id: "+ id);
            return new ResponseEntity<>(this.carService.saveCar(car), HttpStatus.OK);
        } else {
            logger.info("Car with Id: "+ id + " could not be updated." );
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Searches for filtered Cars")
    @GetMapping(value = "/cars", params = "filter",
            headers = HttpHeaders.ACCEPT + "=" + MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Car>> searchCar(@RequestParam(value = "filter") String jsonRequestString) {
        logger.info("searching car with filter:  "+ jsonRequestString);

        CarFilterDto carFilterDto;
        try {
            carFilterDto = getCarFilterDto(jsonRequestString);
            logger.info("successfully converted from json to CarFilterDto");
        } catch (JsonProcessingException e) {
            logger.error("Exception happened while trying to convert following json: "+ jsonRequestString);
            logger.error("",e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        LocalDate startDate = carFilterDto.getStartDate();
        LocalDate endDate = carFilterDto.getEndDate();

        List<Car> cars = this.carService.searchCarMaster(carFilterDto, startDate, endDate);
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @Operation(summary = "Rents a Car")
    @PostMapping("/car/{id}/rental")
    public ResponseEntity<Rental> createRental(@PathVariable("id") long carId, @Valid @RequestBody RentalDto rentalDto) {
        logger.info("Trying to create Rental for car with id: "+ carId);
        Optional<Car> car = this.carService.getCar(carId);

        if (car.isPresent()) {
            logger.info("Car with id: " + carId + " was found");
            if (!RentalDateValidatorService.isRentable(car.get(), rentalDto.getStartDate(), rentalDto.getEndDate())) {
                logger.info("Given Dates are not complete or not valid!");
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            Rental rental = this.rentalService.createRentalForCar(rentalDto, car.get());
            logger.info("For Car with id: " + carId + " rental was successfully created.");
            return new ResponseEntity<>(this.rentalService.saveRental(rental), HttpStatus.CREATED);

        } else {
            logger.info("Car with id: " + carId + " was not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private CarFilterDto getCarFilterDto(String jsonRequestString) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.readValue(jsonRequestString, CarFilterDto.class);
    }
}

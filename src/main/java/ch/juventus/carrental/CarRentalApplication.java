package ch.juventus.carrental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** Represents CarRentalApplication main class that starts up the  Spring Application
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@SpringBootApplication
public class CarRentalApplication {
	public static void main(String[] args) {
		SpringApplication.run(CarRentalApplication.class, args);
	}

}

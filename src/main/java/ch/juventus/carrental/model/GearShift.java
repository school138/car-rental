package ch.juventus.carrental.model;

/** Represents GearShift
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public enum GearShift {
    AUTOMATIC,
    MANUAL
}

package ch.juventus.carrental.model;

/** Represents Type of Car
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
public enum Type {
    CABRIO,
    LIMOUSINE,
    SUV,
    MINIBUS,
    COUPE,
    ESTATE
}

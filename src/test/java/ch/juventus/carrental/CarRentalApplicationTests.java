package ch.juventus.carrental;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
/** Represents CarRentalApplicationTests
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@SpringBootTest
class CarRentalApplicationTests {

	@Test
	void contextLoads() {
	}

}

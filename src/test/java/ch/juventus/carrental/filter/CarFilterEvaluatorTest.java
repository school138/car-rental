package ch.juventus.carrental.filter;

import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.GearShift;
import ch.juventus.carrental.model.Rental;
import ch.juventus.carrental.model.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/** Represents CarFilterEvaluatorTest
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
class CarFilterEvaluatorTest {


    @BeforeEach
    void setUp() {
    }

    /**
     * Tests if CarFilter and Date are matched/not matched
     * Main focus is here on the date, if the car is available on that given date.
     * The Car Filter is is only here airCondition, since it is not the focus of this test
     */
    @Test
    void isCarAndDateMatch() {
        //Given
        Car firstCar = new Car();
        firstCar.setAirCondition(true);
        Rental firstRental = new Rental();
        firstRental.setStartDate(LocalDate.now());
        firstRental.setEndDate(LocalDate.now().plusDays(5));
        firstCar.setRentals(Set.of(firstRental));

        Car secondCar = new Car();
        secondCar.setAirCondition(true);
        Rental secondRental = new Rental();
        secondRental.setStartDate(LocalDate.now().plusDays(10));
        secondRental.setEndDate(LocalDate.now().plusDays(15));
        secondCar.setRentals(Set.of(secondRental));

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setAirCondition(true);
        LocalDate startDate = LocalDate.now().plusDays(12);
        LocalDate endDate = LocalDate.now().plusDays(13);


        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarAndDateMatch(firstCar, carFilterDto, startDate);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarAndDateMatch(secondCar, carFilterDto, endDate);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /**
     * Tests if CarFilter and Dates are matched/not matched
     * Main focus is here on the dates, if the car is available on the time window of the given dates.
     * The Car Filter is is only here airCondition, since it is not the focus of this test
     */
    @Test
    void isCarAndDatesMatch() {
        //Given
        Car firstCar = new Car();
        firstCar.setAirCondition(true);
        Rental firstRental = new Rental();
        firstRental.setStartDate(LocalDate.now());
        firstRental.setEndDate(LocalDate.now().plusDays(5));
        firstCar.setRentals(Set.of(firstRental));

        Car secondCar = new Car();
        secondCar.setAirCondition(true);
        Rental secondRental = new Rental();
        secondRental.setStartDate(LocalDate.now().plusDays(10));
        secondRental.setEndDate(LocalDate.now().plusDays(15));
        secondCar.setRentals(Set.of(secondRental));

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setAirCondition(true);
        LocalDate startDate = LocalDate.now().plusDays(12);
        LocalDate endDate = LocalDate.now().plusDays(22);


        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarAndDatesMatch(firstCar, carFilterDto, startDate, endDate);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarAndDatesMatch(secondCar, carFilterDto, startDate, endDate);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);

    }

    /** Tests if Car is a match for given AirCondition
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForAirCondition() {
        //Given
        Car firstCar = new Car();
        firstCar.setAirCondition(true);

        Car secondCar = new Car();
        secondCar.setAirCondition(false);

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setAirCondition(true);

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /** Tests if car is a match for the given seats
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForSeats() {
        //Given
        Car firstCar = new Car();
        firstCar.setSeats(5);

        Car secondCar = new Car();
        secondCar.setSeats(6);

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setSeats(List.of(5));

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /** Tests if car is a match for the given Gearshift
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForGearShift() {
        //Given
        Car firstCar = new Car();
        firstCar.setGearShift(GearShift.MANUAL);

        Car secondCar = new Car();
        secondCar.setGearShift(GearShift.AUTOMATIC);

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setGearShift(GearShift.MANUAL);

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /** Tests if car is a match for the given Type
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForType() {
        //Given
        Car firstCar = new Car();
        firstCar.setType(Type.CABRIO);

        Car secondCar = new Car();
        secondCar.setType(Type.SUV);

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setType(List.of(Type.COUPE, Type.CABRIO));

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /** Tests if car is a match for the given SearchQuery
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForSearchQuery() {
        //Given
        Car firstCar = new Car();
        firstCar.setName("Cabrio");

        Car secondCar = new Car();
        secondCar.setName("SUV");

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setSearchQuery("brio");

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(true);
        assertThat(isMatchSecondCar).isEqualTo(false);
    }

    /** Tests if car is a match for the given Price per day
     * Tests also the opposite case
     */
    @Test
    void isCarMatchForPrice() {
        //Given
        Car firstCar = new Car();
        firstCar.setPricePerDay(100.00);

        Car secondCar = new Car();
        secondCar.setPricePerDay(200.00);

        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setMinPricePerDay(150.00);
        carFilterDto.setMaxPricePerDay(250.00);

        //when
        boolean isMatchFirstCar = CarFilterEvaluator.isCarMatch(firstCar, carFilterDto);
        boolean isMatchSecondCar = CarFilterEvaluator.isCarMatch(secondCar, carFilterDto);

        //then
        assertThat(isMatchFirstCar).isEqualTo(false);
        assertThat(isMatchSecondCar).isEqualTo(true);
    }
}
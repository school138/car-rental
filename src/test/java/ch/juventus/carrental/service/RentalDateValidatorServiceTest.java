package ch.juventus.carrental.service;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/** Represents RentalDateValidatorServiceTest
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
class RentalDateValidatorServiceTest {

    /** Tests if Dates are Valid
     *
     */
    @Test
    void areDatesValidTrue() {
        //Given
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(5);

        //when
        boolean isValid = RentalDateValidatorService.areDatesValid(startDate, endDate);

        //then
        assertThat(isValid).isEqualTo(true);

    }

    /** Tests if Dates are not Valid
     *
     */
    @Test
    void areDatesValidFalse() {
        //Given
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.minusDays(5);

        //when
        boolean isValid = RentalDateValidatorService.areDatesValid(startDate, endDate);

        //then
        assertThat(isValid).isEqualTo(false);

    }

    /** Tests if Dates are Valid with those in the Past
     *
     */
    @Test
    void areDatesValidWithDatesInPast() {
        //Given
        LocalDate startDate = LocalDate.now().minusDays(10);
        LocalDate endDate = LocalDate.now().minusDays(5);

        //when
        boolean isValid = RentalDateValidatorService.areDatesValid(startDate, endDate);

        //then
        assertThat(isValid).isEqualTo(false);

    }
}
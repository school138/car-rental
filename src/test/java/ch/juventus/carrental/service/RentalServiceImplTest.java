package ch.juventus.carrental.service;

import ch.juventus.carrental.dto.RentalDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Rental;
import ch.juventus.carrental.model.Type;
import ch.juventus.carrental.repository.RentalRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

/** Represents RentalServiceImplTest
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
class RentalServiceImplTest {

    @Mock
    private RentalRepository rentalRepository;

    private RentalServiceImpl underTest;

    @BeforeEach
    void setUp() {
        underTest = new RentalServiceImpl(rentalRepository);
    }

    /** Tests if Rental can be saved
     *
     */
    @Test
    void canSaveRental() {
        //given
        Rental testRental = getTestRental();

        //when
        underTest.saveRental(testRental);

        //then
        verify(rentalRepository).save(testRental);
    }

    /** Tests if Rental can be deleted with given id
     *
     */
    @Test
    void canDeleteById() {
        //given
        Rental testRental = getTestRental();
        testRental.setId(1L);

        //when
        underTest.deleteById(1L);

        //then
        verify(rentalRepository).deleteById(testRental.getId());
    }

    /** Tests if Rental can be created for a car
     *
     */
    @Test
    void createRentalForCar() {
        //given
        RentalDto testRentalDto = getTestRentalDto();
        //Rental testRental = testRentalDto.toRental();


        //when
        Rental createdRental = underTest.createRentalForCar(testRentalDto, getTestCar());

        //then
        assertThat(createdRental.getStartDate()).isEqualTo(testRentalDto.getStartDate());
        assertThat(createdRental.getEndDate()).isEqualTo(testRentalDto.getEndDate());
        assertThat(createdRental.getTotalPrice()).isEqualTo(600.00);
    }

    /** Tests the total calculated price per day
     *
     */
    @Test
    void calculateTotalPrice() {
        //given
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(5);
        Double pricePerDay = 100.00;

        //then
        Double totalPrice = underTest.calculateTotalPrice(pricePerDay, startDate, endDate);
        assertThat(totalPrice).isEqualTo(600.00);
    }

    /** Test data for RentalDto
     *
     * @return test RentalDto
     */
    private RentalDto getTestRentalDto() {
        RentalDto testRentalDto = new RentalDto();
        testRentalDto.setStartDate(LocalDate.now());
        testRentalDto.setEndDate(LocalDate.now().plusDays(5));
        return testRentalDto;
    }

    /** Test data for Rental object
     *
     * @return test rental object
     */
    private Rental getTestRental() {
        Rental testRental = new Rental();
        testRental.setStartDate(LocalDate.now());
        testRental.setEndDate(LocalDate.now().plusDays(5));
        return testRental;
    }

    /** Test data for Car
     *
     * @return test car object
     */
    private Car getTestCar() {
        Car testCar = new Car();
        testCar.setAirCondition(true);
        testCar.setType(Type.CABRIO);
        testCar.setName("TestCar");
        testCar.setSeats(5);
        testCar.setPricePerDay(100.00);
        return testCar;
    }
}
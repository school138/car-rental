package ch.juventus.carrental.service;

import ch.juventus.carrental.dto.CarFilterDto;
import ch.juventus.carrental.model.Car;
import ch.juventus.carrental.model.Type;
import ch.juventus.carrental.repository.CarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
/** Represents CarServiceImplTest
 * @author Luqmann, Cristoph, Fabian
 * @version 1.0
 */
@ExtendWith(MockitoExtension.class)
class CarServiceImplTest {

    @Mock
    private CarRepository carRepository;

    @Mock
    private RentalService rentalService;

    private CarServiceImpl underTest;

    @BeforeEach
    void setUp() {
        underTest = new CarServiceImpl(carRepository, rentalService);
    }

    /** Tests if all cars can be found
     *
     */
    @Test
    void canGetAllCars() {
        //when
        underTest.getAllCars();

        //then
        verify(carRepository).findAll();
    }

    /** Tests if car can be found with given id
     *
     */
    @Test
    void getCar() {
        //given: some car data
        Car testCar = getTestCar();
        testCar.setId(1L);

        //when
        when(underTest.getCar(1L)).thenReturn(Optional.of(testCar));
        underTest.getCar(1L);

        //then
        verify(carRepository).findById(testCar.getId());
    }

    /** Tests if car can be saved
     *
     */
    @Test
    void canSaveCar() {
        //given: some car data
        Car testCar = getTestCar();

        //when
        underTest.saveCar(testCar);

        //then
        verify(carRepository).save(testCar);
    }

    /** Tests if car can be deleted with given id
     *
     */
    @Test
    void canDeleteById() {
        //given: some car data
        Car testCar = getTestCar();
        testCar.setId(1L);

        //when
        when(underTest.getCar(1L)).thenReturn(Optional.of(testCar));
        underTest.deleteById(1L);

        //then
        verify(carRepository).deleteById(testCar.getId());
    }

    /** Tests if correct car is searched
     *
     */
    @Test
    void useCorrectSearchCarMethod() {
        //Given
        Car testCar = getTestCar();
        CarFilterDto carFilterDto = getTestCarFilterDto();
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(10);

        //when
        when(underTest.searchCarMaster(carFilterDto, startDate, endDate)).thenReturn(List.of(testCar));
        underTest.searchCarMaster(carFilterDto, startDate, endDate);

        //then
        List<Car> list = underTest.searchCarWithTwoDates(carFilterDto, startDate, endDate);
        assertThat(list.size()).isEqualTo(1);


    }

    /** Test data for CarFilterDto
     *
     * @return test carFilterDto
     */
    private CarFilterDto getTestCarFilterDto() {
        CarFilterDto carFilterDto = new CarFilterDto();
        carFilterDto.setAirCondition(true);
        carFilterDto.setType(List.of(Type.CABRIO));
        carFilterDto.setSearchQuery("TestCar");
        carFilterDto.setSeats(List.of(4, 5));
        carFilterDto.setMaxPricePerDay(150.00);
        carFilterDto.setMinPricePerDay(50.00);
        return carFilterDto;
    }

    /** Test data for Car
     *
     * @return test car object
     */
    private Car getTestCar() {
        Car testCar = new Car();
        testCar.setAirCondition(true);
        testCar.setType(Type.CABRIO);
        testCar.setName("TestCar");
        testCar.setSeats(5);
        testCar.setPricePerDay(100.00);
        return testCar;
    }

}